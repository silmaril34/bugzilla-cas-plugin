# -*- Mode: perl; indent-tabs-mode: nil -*-
#
# Copyright (c) 2014, TRIOLOGY GmbH
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice,
#    this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
# ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#
# http://www.scm-manager.com

package Bugzilla::Extension::CAS;
use strict;
use warnings;
use parent qw(Bugzilla::Extension);

use Bugzilla::Constants;
use Bugzilla::Error;
use Bugzilla::Group;
use Bugzilla::User;
use Bugzilla::User::Setting;
use Bugzilla::Util qw(diff_arrays html_quote);
use Bugzilla::Status qw(is_open_state);
use Bugzilla::Install::Filesystem;

# See bugmail_relationships.
use constant REL_CAS => -127;

our $VERSION = '1.0';

sub auth_login_methods {
    my ($self, $args) = @_;
    my $modules = $args->{modules};
    if (exists $modules->{CAS}) {
        $modules->{CAS} = 'Bugzilla/Extension/CAS/Auth/Login.pm';
    }
}

sub config_add_panels {
    my ($self, $args) = @_;
    my $modules = $args->{panel_modules};
    $modules->{CAS} = "Bugzilla::Extension::CAS::Config";
}

sub config_modify_panels {
    my ($self, $args) = @_;
    my $panels = $args->{panels};
    
    # Add the "CAS" auth methods.
    my $auth_params = $panels->{'auth'}->{params};
    my ($info_class)   = grep($_->{name} eq 'user_info_class', @$auth_params);

    push(@{ $info_class->{choices} },   'CAS');
    push(@{ $info_class->{choices} },   'CGI,CAS');
}

sub page_before_template {
    my ($self,$args) = @_;
    my $page_id = $args->{'page_id'};
    my $vars    = $args->{'vars'};
    
    if ($page_id eq "cas-logout.html") {
    	$vars->{'logoutUrl'} = Bugzilla::Extension::CAS::Auth::Login::getLogoutUrl();
    	Bugzilla->logout();
    }
}


# This must be the last line of your extension.
__PACKAGE__->NAME;
