Bugzilla CAS-Plugin
==============================
This plugin is for usage with Bugzilla (http://www.bugzilla.org).

Issue tracker: [https://bitbucket.org/triologygmbh/scm-manager-plugins/issues] (https://bitbucket.org/triologygmbh/scm-manager-plugins/issues)
Please set component to "bugzilla-cas-plugin".

Wiki: [https://bitbucket.org/triologygmbh/scm-manager-plugins/wiki/bugzilla_cas_plugin] (https://bitbucket.org/triologygmbh/scm-manager-plugins/wiki/bugzilla_cas_plugin)

Support that makes the difference
https://www.scm-manager.com/support
info@scm-manager.com
