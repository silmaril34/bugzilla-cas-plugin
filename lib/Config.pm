# -*- Mode: perl; indent-tabs-mode: nil -*-
#
# Copyright (c) 2014, TRIOLOGY GmbH
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice,
#    this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
# ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#
# http://www.scm-manager.com

package Bugzilla::Extension::CAS::Config;
use strict;
use warnings;

use Bugzilla::Config::Common;

our $sortkey = 1010;

sub get_param_list {
    my ($class) = @_;

    my @param_list = (
    {
        name => 'CASserver',
        type => 't',
        default => 'https://localhost:8080/cas',
    },
    {
    	name => 'CASserverCert',
    	type => 't',
    	default => '',
    },
    {
        name => 'CASprotocol',
        type => 's',
        #choices => ['CAS1', 'CAS2', 'SAML11'],
        choices => ['SAML11'],
        default => 'SAML11',
        checker => \&check_multi
    },
    {
    	name => 'CASfullNameAttribute',
    	type => 't',
    	default => 'cn'
    },
    {
    	name => 'CASmailAttribute',
    	type => 't',
    	default => 'mail'
    },
    {
    	name => 'CAStimeTolerance',
    	type => 't',
    	default => '1000',
    	checker => \&check_numeric
    },
    );
    return @param_list;
}

1;
